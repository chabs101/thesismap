<head>
<link rel="stylesheet" href="./leaflet-js/leaflet.css" />
<!-- <link rel="stylesheet" href="./bootstrap-native/assets/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="./bootstrap-4.4.1-dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="./fontawesome/css/all.css"/>
<style>
  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }
  html,body {
    margin :0;
    overflow-y: hidden;
    overflow-x: hidden;
  }
  #mymap {
    left:0;
    top:0;
      width: 100%;
      height: 100%;
      position: absolute;
   }

   .left-tools{
    border-radius: 5px 0px 0px 5px;
    right:0;
    height:100px;
    width:50px;
    position:absolute;
    z-index:500;
    margin-top: 5%;
  }

  @keyframes fade { 
    from { opacity: 0.5; } 
  }
  .navbar {
    z-index: 500;
  }
  .blinking {
    animation: pulse 1s infinite alternate;
  }

  @keyframes pulse {
      0% {
        fill: rgba(255,0,0,.3);
        /*opacity: 0;*/
      }
      100% {
        stroke-width:2px;
        fill: rgba(255,0,0,0.9);
        /*opacity: .2;*/
      }
  }

  .btn-zoom {
    width:50px;
    height:50px;
    font-size: 25px;
    border: 0;
  }

  .btn-zoom:active:focus {
    outline: none;
    border: 0;
    box-shadow: none;
  }
  .navbar-nav, a:hover {
    text-decoration: none;
    color: white;
  }
  .navbar-nav, a {
    color: white;
  }
  .modal-left {
    min-width: 0;
    width: auto !important;
  }

  .modal-left .modal-dialog,
  .modal.right .modal-dialog {
    margin: auto;
    width: 320px;
    height: 100%;
    -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
         -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
  }

  .modal-left .modal-content,
  .modal.right .modal-content {
    height: 100%;
    overflow-y: auto;
  }
  
  .modal-left .modal-body,
  .modal.right .modal-body {
    padding: 15px 15px 80px;
  }

  .modal-left.fade .modal-dialog{
    left: 0;
    -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
       -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
         -o-transition: opacity 0.3s linear, left 0.3s ease-out;
            transition: opacity 0.3s linear, left 0.3s ease-out;
  }

  .my-label {
  position: absolute;
  font-size:13px;
  font-weight: bold;
  letter-spacing: 0.2px;
  color:#0062cc !important;
  background-color:transparent !important;
  box-shadow:none;
  border:none;
  }

  .leaflet-tooltip-right:before {
    border-right-color:#0062cc !important;
  }

  .m-icon {
    color:#0062cc !important;
  }

</style>
</head>
<body>
<nav class="navbar bg-dark navbar-dark">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Barangay Manambia</a>
    </div>
<!--     <ul class="nav navbar-nav">
      <li><a href="#">Map</a></li>
    </ul> -->
    <ul class="nav navbar-nav navbar-right">
      <li>
        <a href="#" data-toggle="modal" data-target="#loginModal" id="loginBtn">
          <i class="fa fa-user"></i> Login
        </a>
      </li>
    </ul>
  </div>
</nav>

<div class="container-fluid">
  <div id="mymap"></div>
<div class="bg-dark left-tools">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" title="Zoom In" id='zoomIn'>+</button>
    </div>
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" title="Zoom Out" id='zoomOut'>-</button>
    </div>
  </div>
</div>

<div class="bg-dark left-tools" style="margin-top: 13%; ">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" data-toggle="modal" data-target="#markerModal" title="Add Marker" id="markerBtn"><span class="fa fa-map-marker-alt"></span>
      </button>
    </div>
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" data-toggle="modal" data-target="#poylgonModal" title="Draw a polygon" id="polygonBtn">
        <span class="fa fa-draw-polygon"></span>
      </button>
    </div>
  </div>
</div>

</div>




<!-- Modal Login -->
<div id="loginModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title text-center" id="exampleModalLabel">Login</h2>
      </div>
      <div class="modal-body">
        <h5 style="color:red;" id="invalid_credentials">Invalid Username or Password.</h5>
        <form id="formLogin">
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="text" class="form-control" id="input_email" name="input_email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="input_password" name="input_password" placeholder="Password">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="modalLoginBtn">Login</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Login End-->

  <div class="modal modal-left fade" id="markerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Add Marker</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <form>
            <div class="form-group">
              <label class="error-marker-display" style="color:red;">Please input marker name.</label>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Marker Name</label>
              <input type="text" class="form-control" id="markerNameId" placeholder="Enter Marker Name">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Icon</label>
              <div class="form-group form-check">
                <div class="row">

                    <div class="col-sm-2">
                      <input type="checkbox" class="form-check-input f-icon">
                      <label class="form-check-label"><span class="fa fa-home"></span></label>
                    </div>

                    <div class="col-sm-2">
                      <input type="checkbox" class="form-check-input f-icon">
                      <label class="form-check-label"><span class="fa fa-store-alt"></span></label>
                    </div>

                    <div class="col-sm-2">
                      <input type="checkbox" class="form-check-input f-icon">
                      <label class="form-check-label"><span class="fa fa-map-marker-alt"></span></label>
                    </div>

                </div>
              </div>
            </div>

            <button type="submit" id="markerSubmitBtn" class="btn btn-primary">Save</button>
          </form>
        </div>

      </div>
    </div>
  </div>


  <div class="modal modal-left fade" id="poylgonModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Draw Polygon</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <button id="undoBtn">Undo</button>
        </div>

      </div>
    </div>
  </div>

<script src="./leaflet-js/leaflet.js"></script>
<!-- <script src="bootstrap-native/assets/js/bootstrap-native.js"></script> -->
<script src="./bootstrap-4.4.1-dist/js/jquery.js"></script>
<script src="./bootstrap-4.4.1-dist/js/bootstrap.js"></script>
<script type="module" src="./services/Authenticate.js"></script>
<script>
  var activeBtn = 0;
  var marker_count = 0;
  var mymarker = [];
  var mypolygon = null;
  var mypolygon_arr = [];
  var polygon_count = 0;
  var marker_icon = "fa fa-map-marker-alt";
  var marker_name = $('#markerNameId');
</script>

<script type="module">
  import Auth from './services/Authenticate.js';
  $('#invalid_credentials').hide();
  $('.error-marker-display').hide();

  $('#modalLoginBtn').on('click', async function() {
    var arr = {};
    var input_email     = document.getElementById('input_email');
    var input_password  = document.getElementById('input_password');

    arr["input_email"]    = input_email.value;
    arr["input_password"] = input_password.value;

    console.log(arr)
    const callback = await Auth.authenticate('/Http/Login.php',arr);

        if(!callback.error) {
          if(callback.data[0].success) {
            $('#invalid_credentials').hide();
            $('#loginModal').modal('hide');
            document.getElementById("formLogin").reset();

          }else {
            $('#invalid_credentials').show();

          }
        }
  });

  $("#undoBtn").on('click', function() {

    if(mypolygon_arr.length > 0) {

      mypolygon_arr.pop();
      mypolygon.remove();
      polygon_count = mypolygon_arr.length;
      mypolygon = L.polygon([mypolygon_arr])
      .setStyle({className:'blinking',color:'red',weight:0.5})
      .addTo(map);
    }

  });

  $("#markerBtn").on('click', function() {
    activeBtn = 1;
    document.getElementById('mymap').style.cursor = '';
      if(mypolygon_arr.length > 0) {
        deletePolygon();
      }

  });

   $("#polygonBtn").on('click', function() {
    activeBtn = 2;
    document.getElementById('mymap').style.cursor = 'crosshair';
    disableMarker();
  });

   $(".close").on('click', function() {
      if(activeBtn == 1) {
        disableMarker();
      
      }

      if(activeBtn == 2) {
        
        if(mypolygon_arr.length > 0) {
          
        }

      }

    document.getElementById('mymap').style.cursor = '';
    activeBtn = 0;
  });

  $("#polygonBtn,#markerBtn").on('click', function() {
      setTimeout(()=> {
      $('.modal-backdrop').removeClass('modal-backdrop');
      $('.modal').modal('hide');
      },0.5);
  });

  $(".f-icon").on('click', function() {
    if($(this).prop("checked")) {
      $(".f-icon").prop('checked', false);
      $(this).prop('checked', true);
      marker_icon = $(this).next('label').find('span').attr('class');
    }else {
      marker_icon = "fa fa-map-marker-alt";
    }

  });

  $("#markerSubmitBtn").on('click',function() {
      // JSON.stringify(mar)
        mymarker.forEach(function(item, index, arr){
            console.log(mymarker[index]._latlng);
          console.log(mymarker[index]._tooltip._content);
        });
  });

   $("#polygonSubmitBtn").on('click',function() {
      // JSON.stringify(mar)
        mymarker.forEach(function(item, index, arr){
            console.log(mymarker[index]._latlng);
          console.log(mymarker[index]._tooltip._content);
        });
  }); 
</script>

<script>


var map = L.map('mymap',{
    center: [8.4351,126.1442],
    scrollWheelZoom: false,
    zoomControl: false,
    zoom: 14
});

var mb = L.tileLayer('map/{z}/{x}/{y}.png',{
    minZoom:14,
    maxZoom:17
}).addTo(map);

var asdas = L.polygon([
    [8.433319054311589, 126.13875389099123],
    [8.43730945682969, 126.14021301269533],
    [8.436630242290462, 126.14665031433107],
    [8.430517257707782, 126.14639282226564]
])
.setStyle({className:'blinking',fillColor:'transparent',color:'transparent'})
.addTo(map);

document.getElementById('zoomIn').addEventListener('click', function () {
    map.setZoom(map.getZoom() + 1);
});

document.getElementById('zoomOut').addEventListener('click', function () {
    map.setZoom(map.getZoom() - 1);
});

map.on('click', function(e){
  console.log(activeBtn);
  var coord = e.latlng;
  var lat = coord.lat;
  var lng = coord.lng;

  
  console.log("You clicked the map at latitude: " + lat + " and longitude: " + lng);

    if( activeBtn == 1 ) {
      if(marker_name.val().length > 0 && $('.f-icon:checked').length > 0){

        faIcon = L.divIcon({
            html: '<i class="'+marker_icon+' fa-2x m-icon"></i>',
            className: 'myDivIcon'
        });
       mymarker[marker_count] = new L.Marker(e.latlng, {
                     draggable: true,
                     icon : faIcon
                   });
       mymarker[marker_count].addTo(map);
       mymarker[marker_count].bindTooltip(marker_name.val(), {permanent: true, className: "my-label", direction:'right',offset:[25,5] });
        mymarker[marker_count].bindPopup("latitude: " + lat + " <br> longitude: " + lng + '<button onclick="deleteMarker(\''+marker_count+'\')">x</button>');
        marker_name.val("");
        // var popup = L.popup()
        // .setLatLng(coord)
        // .setContent("latitude: " + lat + " <br> longitude: " + lng)
        // .openOn(map); 
      // setTimeout(()=> {popup.remove();},500);
      mymarker[marker_count].on('dragend', function (dragend) {
          // updateLatLng(mymarker[marker_count].getLatLng().lat, mymarker[marker_count].getLatLng().lng);
          this.bindPopup("This marker, latitude: " + this.getLatLng().lat + " <br> longitude: " + this.getLatLng().lng);
          });
        marker_count++;
        $('.error-marker-display').hide();
      }else {
        if(!(marker_name.val().length > 0)) {
          $('.error-marker-display').text("Please input marker name").show();

        }else if(!($('.f-icon:checked').length > 0)) {
          $('.error-marker-display').text("Please select marker icon.").show();
        }
      }

    }else if( activeBtn == 2 ) {
        console.log(mypolygon_arr.length);
      if(mypolygon_arr.length == 0) {
            mypolygon_arr[polygon_count] = e.latlng;
            mypolygon = L.polygon([mypolygon_arr])
            .setStyle({className:'blinking',color:'red',weight:0.5})
            .addTo(map);
      }else {
            mypolygon_arr[polygon_count] = e.latlng;
            mypolygon.remove();
            mypolygon = L.polygon([mypolygon_arr])
            .setStyle({className:'blinking',color:'red',weight:0.5})
            .addTo(map);
            mypolygon.bindPopup('<button onclick="deletePolygon()">x</button>');
      }
      polygon_count++;
    }else {


    }

  
  });

function updateLatLng(lat,lng,reverse) {
  if(reverse) {
  marker.setLatLng([lat,lng]);
  map.panTo([lat,lng]);
  } else {
  document.getElementById('latitude').value = marker.getLatLng().lat;
  document.getElementById('longitude').value = marker.getLatLng().lng;
  map.panTo([lat,lng]);
  }

}

function disableMarker() {
  marker_icon = "fa fa-map-marker-alt";
  mymarker.forEach(function(item, index, arr){
    mymarker[index].dragging.disable();
  });
}

function deleteMarker(marker_id) {
  mymarker[marker_id].remove();
  index = mymarker.indexOf(marker_id);
  mymarker.splice(index,1);
  marker_count = mymarker.length;

}

function deletePolygon() {

  mypolygon.remove();
  mypolygon_arr.length = 0;
  polygon_count = 0;
  mypolygon = null;
}
</script>
</body>