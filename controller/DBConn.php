<?php
require 'Bcrypt.php';

class DBConn {
	private $type 	= 'localhost';
	private $user 	= 'root';
	private $pass 	= '';
	private $dbname = 'manambiadb';
	public $conn 	= '';
	private $result = '';

	public function __construct() {
		$this->db();
	}

	function db() {
		$this->conn = mysqli_connect($this->type,$this->user,$this->pass,$this->dbname);
		
		if (!$this->conn)
		  {
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		  }
	} 

	function authenticate($username, $password) {
		$result = mysqli_query($this->conn,"SELECT * FROM dt_user WHERE username='".$username."' ");
		if( $result->num_rows > 0 ) {
			$row = mysqli_fetch_assoc($result);
			$decrypt = new Bcrypt();
			if($decrypt->verify($password,$row['password'])) {

				$_SESSION['fullname'] = $row['first_name'] .' '. $row['last_name'];
				$_SESSION['first_name'] = $row['first_name'] .' '. $row['last_name'];
				$_SESSION['user'] = [
									"id" 		=> $row['user_id'],
									"first_name" => $row['first_name'],
									"fullname" 	=> $row['first_name'] .' '. $row['last_name']
									];
				return true;

			}
		}
			return false;
		
	}

	function select($table,$column="*", $where = "") {
		$query = "SELECT $column FROM $table";
		$query .= trim($where)=="" ? "" : " WHERE $where";

		$this->result = mysqli_query($this->conn,$query );

		if(!$this->result) {
			printf("Error: %s\n",mysqli_error($this->conn));
		}
	}

	function result() { 
		$resultset = [];
			while($row = mysqli_fetch_array($this->result,MYSQLI_ASSOC)) {
				$resultset[] = $row;
			}
		return $resultset;

	}

	function row() { 
		$row = mysqli_num_rows($this->result);
		return $row;

	}
	function rawQuery($query) {
		$result = mysqli_query($this->conn,$query );

		if ($result == TRUE) {
		    return true;
		} 

		if(!$this->result) {
			printf("Error: %s\n",mysqli_error($this->conn));
		}

	}

	function softDelete($table,$column,$id) {
		mysqli_query($this->conn,"UPDATE $table SET isdeleted='1' WHERE $column='".$id."'");


	}

	function permaDelete($table,$column,$id) {
		mysqli_query($this->conn,"DELETE FROM $table WHERE $column='".$id."' ");
	}

}

?>