<?php
  include('inc_common/session.php');
?>
<head>
<link rel="stylesheet" href="../assets/leaflet-js/leaflet.css" />
<!-- <link rel="stylesheet" href="../assets/bootstrap-native/assets/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="../assets/bootstrap-4.4.1-dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="../assets/fontawesome/css/all.css"/>
<link href="../assets/summernote-lite.min.css" rel="stylesheet">
<link href="../assets/autocomplete/auto-complete.css" rel="stylesheet">
<link href="../assets/datatable/datatable.css" rel="stylesheet">

<style>
  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }

  .dropdown-item {
        padding: .10rem .5rem !important;
  }
 
 body {
     background-image: url(../assets/world_map_bg.jpg); 
     background-size: 100%;
  }

  html,body {
    margin :0;
    overflow-y: hidden;
    overflow-x: hidden;
  }
  #mymap {
      left:0;
      top:0;
      width: 100%;
      height: 100%;
      position: absolute;
   }

   .left-tools{
    border-radius: 5px 0px 0px 5px;
    right:0;
    height:100px;
    width:50px;
    position:absolute;
    z-index:500;
    margin-top: 5%;
  }

  @keyframes fade { 
    from { opacity: 0.5; } 
  }
  .navbar {
    z-index: 500;
  }
  .blinking {
    animation: pulse 1s infinite alternate;
  }

  @keyframes pulse {
      0% {
        /*fill: rgba(255,0,0,.3);
        /*opacity: 0;*/
      }
      100% {
        stroke-width:2px;
        /*fill: rgba(255,0,0,0.9);*/
        opacity: .4;
      }
  }

  .btn-zoom {
    width:50px;
    height:50px;
    font-size: 25px;
    border: 0;
  }

  .btn-zoom:active:focus {
    outline: none;
    border: 0;
    box-shadow: none;
  }
  .navbar-nav, a:hover {
    text-decoration: none;
    color: white;
  }
  .navbar-nav, a {
    color: white;
  }
  .modal-left {
    min-width: 0;
    width: auto !important;
  }

  .modal-left .modal-dialog,
  .modal.right .modal-dialog {
    margin: auto;
    width: 320px;
    height: 100%;
    -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
         -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
  }

  .modal-left .modal-content,
  .modal.right .modal-content {
    height: 100%;
    overflow-y: auto;
  }
  
  .modal-left .modal-body,
  .modal.right .modal-body {
    padding: 15px 15px 80px;
  }

  .modal-left.fade .modal-dialog{
    left: 0;
    -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
       -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
         -o-transition: opacity 0.3s linear, left 0.3s ease-out;
            transition: opacity 0.3s linear, left 0.3s ease-out;
  }

  .my-label {
  position: absolute;
  font-size:13px;
  font-weight: bold;
  letter-spacing: 0.2px;
  color:#0062cc !important;
  background-color:transparent !important;
  box-shadow:none;
  border:none;
  }

  .leaflet-tooltip-right:before {
    border-right-color:#0062cc !important;
  }

  .m-icon {
    color:#0062cc !important;
  }

   .color-orange {
    color:#ff8000;
  }

  .navbar-right > li {
      margin-right: 15px;
  }

  .vmap {
    position: absolute;
    z-index: 550;
    top: 50%;
    left: 47%;
    background-color: #343a40 !important;
    color:#fff;
    border:1px solid #ff8000;
    font-family: 'Courier New';

  }

</style>
</head>
<body>
<a class="btn btn-info vmap" href="/thesismap">View Map</a>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <ul class="nav navbar-nav navbar-right">
    <li>
        <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">

          </ul>
          <span class="navbar-text">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <!-- <i class="fa fa-user color-orange"></i> -->&nbsp;&nbsp;Profile
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink">
                  <a href="./view/edit-map.php" data-toggle="modal" data-target="#poylgonModal" title="Draw purok covered" id="polygonBtn" class="dropdown-item">
                    <i class="fa fa-home color-orange"></i> Purok
                  </a>
                  <a href="#" data-toggle="modal" data-target="#markerModal" title="Add Marker" id="markerBtn" class="dropdown-item">
                    <i class="fa fa-user color-orange"></i> Household
                  </a>
                </div>
              </li>
            </ul>
          </span>
        </div>
    </li>

    <li>
      <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">

          </ul>
          <span class="navbar-text">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown">
                <a href="#" data-toggle="modal" data-target="#floodRecordModal">
                  Flood Record
                </a>
              </li>
            </ul>
          </span>
        </div>
    </li>  

    <li>
      <div class="collapse navbar-collapse" id="navbarText">
          <ul class="navbar-nav mr-auto">

          </ul>
          <span class="navbar-text">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown">
                <a href="#" data-toggle="modal" data-target="#helpModal">
                  Help
                </a>
              </li>
            </ul>
          </span>
        </div>
    </li>  

    <li>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">

        </ul>
        <span class="navbar-text">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-cog color-orange"></i>&nbsp;Settings
              </a>
              <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" data-toggle="modal" data-target="#ChangePassModal">Change Password</a>
              </div>
            </li>
          </ul>
        </span>
      </div>
    </li>

  </ul>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">

    </ul>
    <span class="navbar-text">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a href="../controller/api/session-destroy.php">
            <i class="fa fa-sign-out-alt color-orange"></i> Log Out
          </a>
        </li>
      </ul>
    </span>
  </div>
</nav>

<div class="container-fluid">
  <div id="mymap"></div>
<div class="bg-dark left-tools">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" title="Zoom In" id='zoomIn'>+</button>
    </div>
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" title="Zoom Out" id='zoomOut'>-</button>
    </div>
  </div>
</div>

<!-- <div class="bg-dark left-tools" style="margin-top: 13%; ">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" data-toggle="modal" data-target="#markerModal" title="Add Marker" id="markerBtn"><span class="fa fa-map-marker-alt"></span>
      </button>
    </div>
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" data-toggle="modal" data-target="#poylgonModal" title="Draw a polygon" id="polygonBtn">
        <span class="fa fa-draw-polygon"></span>
      </button>
    </div>
  </div>
</div> -->

</div>


<!-- Modal change password -->
<div id="ChangePassModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title text-center" id="exampleModalLabel">Change Password</h2>
      </div>
      <div class="modal-body">
        <h5 style="color:red;" id="invalid_password">Invalid Password.</h5>
        <form id="formChangePassword">
          <div class="form-group">
            <label for="exampleInputPassword1">Old Password</label>
            <input type="password" class="form-control" id="input_old_password" name="input_old_password" placeholder="Password">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="input_password" name="input_password" placeholder="Password">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Confirm Password</label>
            <input type="password" class="form-control" id="input_confirm_password" name="input_confirm_password" placeholder="Password">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="modalChangePassBtn">Login</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal change password End-->

<!-- Modal flood record -->
<div id="floodRecordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel">FLOOD RECORD</h5>
      </div>
      <div class="modal-body">
      <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#addEditFloodRecordModal" id="addeditfloodRecord">
        Add Record
      </button>
      <br><br>
        <div class="table-responsive">
          <table class="table" id="flood-record-tbl" style="width:100%;">
            <thead>
              <tr class="headings">
                <th class="column-title">Place </th>
                <th class="column-title">Description</th>
                <th class="column-title">start - end</th>
                <th class="column-title">Date Happen</th>
                <th class="column-title">Date Inserted</th>
                <th class="column-title">Action</th>
                </th>
              </tr>
            </thead>

            <tbody>
              
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="addEditFloodRecordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title text-center" id="exampleModalLabel">Flood Record Modal</h2>
      </div>
      <div class="modal-body">
        <h5 style="color:red;" id="invalid_flood_display"></h5>
        <form id="formFloodRecord">
          <div class="form-group">
            <label for="exampleInputPassword1">Place</label>
            <input type="hidden" class="form-control" name="flood_id" placeholder="Place">
            <input type="text" class="form-control" name="flood_name" placeholder="Place">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Description</label>
            <input type="text" class="form-control" name="flood_desc" placeholder="Description">
          </div>
          <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="exampleInputPassword1">start</label>
                  <input type="datetime-local" class="form-control" name="start" id="startDate">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="exampleInputPassword1">end</label>
                  <input type="datetime-local" class="form-control" name="end">
                </div>
              </div>
          </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="exampleInputPassword1">Date Happen</label>
                  <input type="date" class="form-control" name="date_happen">
                </div>
              </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submitFloodRecordBtn">Submit</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal flood record End-->

<!-- Modal Help -->
<div id="helpModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center" style="justify-content: center;">
        <h5 class="modal-title " id="exampleModalLabel">HELP</h5>
      </div>
      <div class="modal-body">
        <p style="text-align: justify;">
          This study will utilize the developmental life cycle to map the Barangay Manambia, Tagbina, Surigao del Sur using GIS Mapping (Geographical Information Systems).
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Help End-->

  <div class="modal modal-left fade" id="markerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">ADD HOUSEHOLD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <form>
            <div class="form-group">
              <label class="error-marker-display" style="color:red;">Please input marker name.</label>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Marker Name</label>
              <input type="text" class="form-control" id="markerNameId" placeholder="Enter Marker Name">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Purok Name</label>
              <input type="text" class="form-control" id="purokName" placeholder="Enter Purok Name">
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Icon</label>
              <div class="form-group form-check">
                <div class="row">

                    <div class="col-sm-2">
                      <input type="checkbox" class="form-check-input f-icon">
                      <label class="form-check-label"><span class="fa fa-home"></span></label>
                    </div>

                    <div class="col-sm-2">
                      <input type="checkbox" class="form-check-input f-icon">
                      <label class="form-check-label"><span class="fa fa-store-alt"></span></label>
                    </div>

                    <div class="col-sm-2">
                      <input type="checkbox" class="form-check-input f-icon">
                      <label class="form-check-label"><span class="fa fa-map-marker-alt"></span></label>
                    </div>

                </div>
              </div>
            </div>
            
            <div class="form-group">
              <label for="exampleInputEmail1">Description</label>
              <div id="summernote"></div>
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1" class="bold">Household Member : </label>
            </div>

            <div class="form-group" id="inputPersonContainer">


            </div>
            <div class="form-group">
              <button type="button" class="form-control btn btn-info" id="addPersonBtn">Add Person</button>
            </div>
            <button type="button" id="markerSubmitBtn" class="btn btn-primary">Save</button>
          </form>
        </div>

      </div>
    </div>
  </div>


  <div class="modal modal-left fade" id="poylgonModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">DRAW PUROK COVERED</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <form>
            <div class="form-group">
              <label class="error-polygon-display" style="color:red;"></label>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Purok Name</label>
              <input type="text" class="form-control" id="descId" name="description" placeholder="Enter Purok Name">
            </div>

            <button type="button" class="btn btn-warning" id="undoBtn">Undo</button>
            <button type="button" id="polygonSubmitBtn" class="btn btn-primary">Save</button>
          </form>

        </div>

      </div>
    </div>
  </div>


  <div class="modal fade" id="floodLevelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Update Level Indicator</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <form>
              <div class="form-group">
              <button type="button" class="btn btn-primary form-control btn-indicator" data-fill="#00bfff" data-distance="18" style="background-color:#00bfff;border-color:#00bfff;">Below Normal</button>
              </div>
              <div class="form-group">
              <button type="button" class="btn btn-primary form-control btn-indicator" data-fill="#00ff00" data-distance="37" style="background-color:#00ff00;border-color:#00ff00;">Normal</button>
              </div>
              <div class="form-group">
              <button type="button" class="btn btn-primary form-control btn-indicator" data-fill="#ffff00" data-distance="149" style="background-color:#ffff00;border-color:#ffff00;">Above Normal</button>
              </div>
              <div class="form-group">
              <button type="button" class="btn btn-primary form-control btn-indicator" data-fill="#ff8000" data-distance="224" style="background-color:#ff8000;border-color:#ff8000;">Severe</button>
              </div>
              <div class="form-group">
              <button type="button" class="btn btn-primary form-control btn-indicator" data-fill="#ff0000" data-distance="300" style="background-color:#ff0000;border-color:#ff0000;">Extreme</button>
              </div>

          </form>

        </div>

      </div>
    </div>
  </div>

<script src="../assets/leaflet-js/leaflet.js"></script>
<!-- <script src="bootstrap-native/assets/js/bootstrap-native.js"></script> -->
<script src="../assets/bootstrap-4.4.1-dist/js/jquery.js"></script>
<script src="../assets/bootstrap-4.4.1-dist/js/bootstrap.js"></script>

<script src="../assets/summernote-lite.min.js"></script>
<script src="../assets/autocomplete/auto-complete.min.js"></script>
<script src="../assets/datatable/datatable.js"></script>
<script src="../assets/moment.js"></script>
<script src="../assets/sweetalert.js"></script>
<script src="../node_modules/socket.io-client/dist/socket.io.js"></script>

<script>
  var activeBtn = 0;
  var marker_count = 0;
  var mymarker = [];
  var mypolygon = null;
  var mypolygon_arr = [];
  var polygon_count = 0;
  var marker_icon = "fa fa-map-marker-alt";
  var marker_name = $('#markerNameId');
  var purok_name = $('#purokName');
  var markerfromdb = [];
  var polygonfromdb = [];
  var template = $('<div class="row">\
                <div class="col-sm-6">\
                  <label for="exampleInputEmail1">Name</label>\
                  <input type="text" class="form-control personName" placeholder="Enter Name">\
                </div>\
                <div class="col-sm-4">\
                  <label for="exampleInputEmail1">Age</label>\
                  <input type="number" min="1" class="form-control personAge" placeholder="Age">\
                </div>\
                <div class="col-sm-2">\
                <label for="exampleInputEmail1">&nbsp;</label>\
                  <button type="button" class="btn btn-danger btn-sm" onclick="javascript:$(this).parent().parent().remove();">x</button>\
                <div>\
            </div>');
  template.appendTo($('#inputPersonContainer'));
  $('#summernote').summernote({
      placeholder : 'Input text here...',
      tabsize : 2,
      height:100,

  });
</script>

<script type="module">
  import PushData from '../services/PushData.js';
  import PullData from '../services/PullData.js';
  $('.error-marker-display').hide();
  $('.error-polygon-display').hide();
  $('#invalid_password').hide();
  $('.note-toolbar').hide();
  setTimeout(()=>{  $('#mymap,.left-tools').hide();},1000);

// submit flood record start
$(document).on('click','#submitFloodRecordBtn', async function() {
    var arr = {};
    arr['flood_id']   = $('input[name=flood_id]').val();
    arr['flood_name'] = $('input[name=flood_name]').val();
    arr['flood_desc'] = $('input[name=flood_desc]').val();
    arr['start']      = moment.utc($('input[name=start]').val()).local().format();
    arr['end']        = moment.utc($('input[name=end]').val()).local().format();
    arr['date_happen'] =$('input[name=date_happen]').val();

    const callback = await PushData.pushData('/controller/api/save-flood-record.php',arr);
      // console.log(callback)
        if(!callback.error) {
          if(callback.data.success) {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Success.',
              showConfirmButton: false,
              timer: 1500
            });
            $('#formFloodRecord')[0].reset();
            $('#addEditFloodRecordModal').modal('hide');
            tblfloodrecord.ajax.reload();
            return;
          }else {
            Swal.fire({
              position: 'center',
              icon: 'danger',
              title: 'Something Went Wrong.',
              showConfirmButton: false,
              timer: 1500
            });
            console.log("fail");

          }
        }
});

$(document).on('click','.flood-delete-btn',async function() {
    var arr = {};
    arr['id'] = $(this).attr('data-id');

    const callback = await PushData.pushData('/controller/api/delete-flood-record.php',arr);

        if(!callback.error) {
          if(callback.data[0].success) {
            console.log("success");
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Deleted Successfully.',
              showConfirmButton: false,
              timer: 1500
            });
            tblfloodrecord.ajax.reload();
            return;
          }else {
            console.log("fail");
            Swal.fire({
              position: 'center',
              icon: 'danger',
              title: 'Something Went Wrong.',
              showConfirmButton: false,
              timer: 1500
            });

          }
        }
});


// submit flood record end

  const getmarker = async() => {
    const callbackPull = await PullData.pullData('/controller/api/get-all-marker.php');

        if(!callbackPull.error) {

            callbackPull.data.forEach( async function(item, index, arr) {
              // console.log(JSON.parse([callbackPull.data[index].latlng]));
              var icon        = callbackPull.data[index].icon;
              var markername  = callbackPull.data[index].marker_name;
              var purokname   = callbackPull.data[index].purok_name ? "(" + callbackPull.data[index].purok_name + ") - " : "";
              var latlng      = [];
              var _latlng     = JSON.parse(callbackPull.data[index].latlng);
              var marker_id   = callbackPull.data[index].marker_id;
              var person      = callbackPull.data[index][0].person;
              var description = callbackPull.data[index].description ? callbackPull.data[index].description : "";

              for(var parse in _latlng) {
                latlng.push(_latlng[parse]);
              }

              var faIcon = L.divIcon({
                  html: icon,
                  className: 'myDivIcon'
              });

              markerfromdb[index] = new L.Marker(latlng, {
                           icon : faIcon
                         });

               markerfromdb[index].addTo(map);
               markerfromdb[index].bindTooltip(purokname+markername, {permanent: true, className: "my-label", direction:'right',offset:[10,0] });
                markerfromdb[index].bindPopup(description+'<br><br>\
                  <button type="button" class="btn btn-danger btn-sm delete-marker-btn" data-marker-id='+marker_id+' data-index='+index+'>Delete</button>');

            });
          }else{
            console.log("fail");
          }
        
  }
// #00bfff
// #00ff00
// #ffff00
// #ff8000
// #ff0000
  const getpolygon = async() => {
    const callbackPull = await PullData.pullData('/controller/api/get-all-polygon.php');

        if(!callbackPull.error) {

            callbackPull.data.forEach( async function(item, index, arr) {
              var polygon_id  = callbackPull.data[index].polygon_id;
              var _latlng     = JSON.parse(callbackPull.data[index].latlng);
              var description = callbackPull.data[index].description ? callbackPull.data[index].description : "";
              var distance_color = callbackPull.data[index].distance == 18 ? "#00bfff" : (callbackPull.data[index].distance == 37 ? "#00ff00" : callbackPull.data[index].distance == 149 ? "#ffff00" : callbackPull.data[index].distance == 224 ? "#ff8000" : "#ff0000" );
              polygonfromdb[index] = L.polygon(_latlng)
              .setStyle({className:'polygon_'+polygon_id,color:distance_color,weight:0.5,fill:distance_color})
              .bindPopup(description+'<br><br>\
              <button type="button" class="btn btn-primary btn-sm update-polygon-btn" data-polygon-id='+polygon_id+' data-index='+index+'">Update Level Indicator</button>\
              <button type="button" class="btn btn-danger btn-sm delete-polygon-btn" data-polygon-id='+polygon_id+' data-index='+index+'>Delete</button>')
              .addTo(map);
              $('.polygon_'+polygon_id).addClass("blinking");

            });
          }else{
            console.log("fail");
          }
        
  }

  getmarker();
  getpolygon();

  $(document).on('click',".delete-marker-btn", async function() {
    markerfromdb[$(this).attr('data-index')].remove();
    var _index = markerfromdb.indexOf($(this).attr('data-index'));
    markerfromdb.splice(_index,1);
    var arr = {};
    arr['id'] = $(this).attr('data-marker-id');

    const callback = await PushData.pushData('/controller/api/delete-marker.php',arr);

        if(!callback.error) {
          if(callback.data[0].success) {
            console.log("success");
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'HouseSuccess.',
              showConfirmButton: false,
              timer: 1500
            });
            
          }else {
            console.log("fail");

          }
        }
  });

    $(document).on('click',"#modalChangePassBtn", async function() {
      var arr = {};
      arr['old_password']   = $('#input_old_password').val();
      arr['new_password']   = $('#input_password').val();
      if($('#input_confirm_password').val() !== $('#input_password').val()) {
        $('#invalid_password').html("Password do not match.").show();
        console.log("fail");
        return ;
      }

      const callback = await PushData.pushData('/controller/api/change-password.php',arr);
            console.log(callback);
          if(!callback.error) {
            if(callback.data[0].success) {
              console.log("success");
            $('#invalid_password').hide();
             $('#ChangePassModal').modal('hide');
              Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Success.',
                showConfirmButton: false,
                timer: 1500
              });
             document.getElementById("formChangePassword").reset();
            }else {
              var msg = callback.data[0].message ? callback.data[0].message : "Invalid Password";
              $('#invalid_password').html(msg).show();
              console.log("fail");
            }
          }

    });

    $(document).on('click',".update-polygon-btn", async function() {

      $('.btn-indicator').attr('data-polygon-id', $(this).attr('data-polygon-id'));
      $('#floodLevelModal').modal('show');
    });

    $(document).on('click','.btn-indicator', async function() {
    var arr = {};
    arr['polygon_id']   = $(this).attr('data-polygon-id');
    arr['distance']     = $(this).attr('data-distance');
    var fill_color      = $(this).attr('data-fill');
    console.log(fill_color);
    const callback = await PushData.pushData('/controller/api/update-polygon-distance.php',arr);

        if(!callback.error) {
          if(callback.data[0].success) {
            console.log("success");

            $('.polygon_'+arr['polygon_id']).attr({stroke:fill_color,fill:fill_color}).removeClass('blinking').addClass('blinking');

            var socket = io(":4000");
            socket.emit('realtimeData',
              {'realtimeData':true,
              data:{
                indicator  : $(this).attr('data-polygon-id'),
                distance   :$(this).attr('data-distance'),
              }
            });
           $('#floodLevelModal').modal('hide');
          }else {
            console.log("fail");
            Swal.fire({
              position: 'center',
              icon: 'danger',
              title: 'Something Went Wrong.',
              showConfirmButton: false,
              timer: 1500
            });

          }
        }

    });

    $(document).on('click',".delete-polygon-btn", async function() {
    polygonfromdb[$(this).attr('data-index')].remove();
    var _index = polygonfromdb.indexOf($(this).attr('data-index'));
    polygonfromdb.splice(_index,1);
    var arr = {};
    arr['id'] = $(this).attr('data-polygon-id');

    const callback = await PushData.pushData('/controller/api/delete-polygon.php',arr);

        if(!callback.error) {
          if(callback.data[0].success) {
            console.log("success");
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Deleted Successfully.',
              showConfirmButton: false,
              timer: 1500
            });
            // setTimeout(function()
            // {
            //     var redirect_url = "./edit-map";
                
            //     window.location.href = redirect_url;
            // }, 400);
            
          }else {
            console.log("fail");
            Swal.fire({
              position: 'center',
              icon: 'danger',
              title: 'Something Went Wrong.',
              showConfirmButton: false,
              timer: 1500
            });

          }
        }
  });

  $("#undoBtn").on('click', function() {

    if(mypolygon_arr.length > 0) {

      mypolygon_arr.pop();
      mypolygon.remove();
      polygon_count = mypolygon_arr.length;
      mypolygon = L.polygon([mypolygon_arr])
      .setStyle({className:'blinking',color:'#00bfff',weight:0.5,fill:'#00bfff'})
      .addTo(map);
    }

  });

  $("#markerBtn").on('click', function() {
    activeBtn = 1;
    document.getElementById('mymap').style.cursor = '';
      if(mypolygon_arr.length > 0) {
        deletePolygon();
      }

  });

   $("#polygonBtn").on('click', function() {
    activeBtn = 2;
    document.getElementById('mymap').style.cursor = 'crosshair';
    disableMarker();
  });

   $(".close").on('click', function() {
    $('#mymap, .left-tools').hide();
    $('.vmap').show();
      if(activeBtn == 1) {
        disableMarker();
      
      }

      if(activeBtn == 2) {
        
        if(mypolygon_arr.length > 0) {
          
        }

      }

    document.getElementById('mymap').style.cursor = '';
    activeBtn = 0;
  });

   $('#addPersonBtn').on('click',function() {
      var template = $('<div class="row">\
                <div class="col-sm-6">\
                  <label for="exampleInputEmail1">Name</label>\
                  <input type="text" class="form-control personName" placeholder="Enter Name">\
                </div>\
                <div class="col-sm-4">\
                  <label for="exampleInputEmail1">Age</label>\
                  <input type="number" min="1" class="form-control personAge" placeholder="Age">\
                </div>\
                <div class="col-sm-2">\
                <label for="exampleInputEmail1">&nbsp;</label>\
                  <button type="button" class="btn btn-danger btn-sm" onclick="javascript:$(this).parent().parent().remove();">x</button>\
                <div>\
            </div>');

      template.appendTo($('#inputPersonContainer'));
   });

  $("#polygonBtn,#markerBtn").on('click', function() {
    $('#mymap, .left-tools').show();
    $('.vmap').hide();
      setTimeout(()=> {
      $('.modal-backdrop').removeClass('modal-backdrop');
      $('.modal').modal('hide');
      },0.5);
  });

  $(".f-icon").on('click', function() {
    if($(this).prop("checked")) {
      $(".f-icon").prop('checked', false);
      $(this).prop('checked', true);
      marker_icon = $(this).next('label').find('span').attr('class');
    }else {
      marker_icon = "fa fa-map-marker-alt";
    }

  });


  $("#markerSubmitBtn").on('click',function() {
     // if(!($('.personName').val().length > 0) || !($('.personAge').val().length > 0)) {
     //  $('.error-marker-display').text("Please input household member.").show();
     //  return;
     // }
     
     if(mymarker.length > 0) {
        mymarker.forEach(async function(item, index, arr){
         var arr = {};

          arr['icon']         = mymarker[index]._icon.innerHTML;
          arr['latlng']       = JSON.stringify(mymarker[index]._latlng);
          arr['marker_name']  = mymarker[index].marker_name;
          arr['purok_name']   = mymarker[index].purok_name;
          arr['description']  = mymarker[index].summernote;
          arr['person']       = mymarker[index].person;
          console.log(arr);

          const callback = await PushData.pushData('/controller/api/save-marker.php',arr);

              if(!callback.error) {
                if(callback.data[0].success) {
                  console.log("success");
                  if(index == mymarker.length -1) {
                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Success.',
                      showConfirmButton: false,
                      timer: 1500
                    });
                    setTimeout(function()
                    {
                        var redirect_url = "./edit-map";
                        
                        window.location.href = redirect_url;
                    }, 400);
                  }
                }else {
                  console.log("fail");

                }
              }
        });



      }else {
        $('.error-marker-display').text("Please input marker first.").show();
      }

  });

   $("#polygonSubmitBtn").on('click', async function() {

     if(mypolygon_arr.length > 2) {
       var arr = {};

        arr['latlng']       = JSON.stringify(mypolygon_arr);
        arr['description']  = $('#descId').val();
        console.log(arr);
        const callback = await PushData.pushData('/controller/api/save-polygon.php',arr);

            if(!callback.error) {
              if(callback.data[0].success) {
                console.log("success");
                  Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Success.',
                    showConfirmButton: false,
                    timer: 1500
                  });
                  setTimeout(function()
                  {
                      var redirect_url = "./edit-map";
                      
                      window.location.href = redirect_url;
                  }, 400);
              }else {
                console.log("fail");

              }
            }

      }else {
        $('.error-polygon-display').text("Please draw atleast 3 sides first.").show();
      }
  }); 

  $('#descId').on('change', function() {
    mypolygon.bindPopup($(this).val()+'<br><br>\
      <button type="button" class="btn btn-danger btn-sm" onclick="deletePolygon()">delete</button>');
  });
</script>

<script>


var map = L.map('mymap',{
    center: [8.4351,126.1442],
    scrollWheelZoom: false,
    zoomControl: false,
    zoom: 14
});

if(window.navigator.onLine) {
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}{r}.png', {
      attribution: '© OpenStreetMap contributors'
  }).addTo(map);
}else {
  L.tileLayer('../assets/map/{z}/{x}/{y}.png',{
      minZoom:14,
      maxZoom:17
  }).addTo(map);
}


// var asdas = L.polygon([
//     [8.433319054311589, 126.13875389099123],
//     [8.43730945682969, 126.14021301269533],
//     [8.436630242290462, 126.14665031433107],
//     [8.430517257707782, 126.14639282226564]
// ])
// .setStyle({className:'blinking',fillColor:'transparent',color:'transparent'})
// .addTo(map);
// asdas.bindPopup("This marker, latitude: ");

document.getElementById('zoomIn').addEventListener('click', function () {
    map.setZoom(map.getZoom() + 1);
});

document.getElementById('zoomOut').addEventListener('click', function () {
    map.setZoom(map.getZoom() - 1);
});

map.on('click', function(e){
  console.log(activeBtn);
  var coord = e.latlng;
  var lat = coord.lat;
  var lng = coord.lng;

  
  console.log("You clicked the map at latitude: " + lat + " and longitude: " + lng);

    if( activeBtn == 1 ) {
      if(marker_name.val().length > 0 && $('.f-icon:checked').length > 0){

        faIcon = L.divIcon({
            html: '<i class="'+marker_icon+' m-icon"></i>',
            className: 'myDivIcon'
        });
       mymarker[marker_count] = new L.Marker(e.latlng, {
                     draggable: true,
                     icon : faIcon
                   });
       mymarker[marker_count].addTo(map);
       var purokname   = purok_name.val().length ? "(" + purok_name.val() + ") - " : "";
       mymarker[marker_count].bindTooltip(purokname + marker_name.val(), {permanent: true, className: "my-label", direction:'right',offset:[10,0] });
       mymarker[marker_count].summernote  = $('#summernote').summernote('code');
       mymarker[marker_count].purok_name  = purok_name.val();
       mymarker[marker_count].marker_name = marker_name.val();
       mymarker[marker_count].person = [];
       for(var i=0; i<$('.personName').length; i++) {
        mymarker[marker_count].person.push({"name" : $('.personName').eq(i).val(), "age" : $('.personAge').eq(i).val() });
       }
        mymarker[marker_count].bindPopup($('#summernote').summernote('code') + '<button class="btn btn-danger btn-sm" onclick="deleteMarker(\''+marker_count+'\')">REMOVE</button>');
        marker_name.val("");
        purok_name.val("");
        $('#inputPersonContainer').html("");
        template.find('.personName').val("");
        template.find('.personAge').val("");
        template.appendTo($('#inputPersonContainer'));
        $('#summernote').summernote('empty');
        // var popup = L.popup()
        // .setLatLng(coord)
        // .setContent("latitude: " + lat + " <br> longitude: " + lng)
        // .openOn(map); 
      // setTimeout(()=> {popup.remove();},500);
      mymarker[marker_count].on('dragend', function (dragend) {
          // updateLatLng(mymarker[marker_count].getLatLng().lat, mymarker[marker_count].getLatLng().lng);
          // this.bindPopup("This marker, latitude: " + this.getLatLng().lat + " <br> longitude: " + this.getLatLng().lng);
          });
        marker_count++;
        $('.error-marker-display').hide();
      }else {
        if(!(marker_name.val().length > 0)) {
          $('.error-marker-display').text("Please input marker name").show();

        }else if(!($('.f-icon:checked').length > 0)) {
          $('.error-marker-display').text("Please select marker icon.").show();
        }
      }

    }else if( activeBtn == 2 ) {
        console.log(mypolygon_arr.length);
      if(mypolygon_arr.length == 0) {
            mypolygon_arr[polygon_count] = e.latlng;
            mypolygon = L.polygon([mypolygon_arr])
            .setStyle({className:'blinking',color:'#00bfff',weight:0.5,fill:'#00bfff'})
            .addTo(map);
      }else {
            mypolygon_arr[polygon_count] = e.latlng;
            mypolygon.remove();
            mypolygon = L.polygon([mypolygon_arr])
            .setStyle({className:'blinking',color:'#00bfff',weight:0.5,fill:'#00bfff'})
            .addTo(map);
            mypolygon.bindPopup($('#descId').val()+'<br><br>\
              <button type="button" class="btn btn-danger btn-sm" onclick="deletePolygon()">delete</button>');
      }
      polygon_count++;
    }else {


    }

  });

    var tblfloodrecord = $('#flood-record-tbl').DataTable({
        ajax: {
            url: '../controller/api/get-flood-record.php',
            dataSrc: '',
        },
        columns: [
                {
                    data : 'flood_name'         
                },
                {
                    data : 'flood_desc'          
                },
                {
                    data : function(data) {
                        return moment(data.start).format('MMMM D hh:mma') + "-"+ moment(data.end).format('MMMM D hh:mma');
                    }            
                },
                {
                    data : function(data) {
                        return moment(data.date_happen).format('MMMM D, YYYY');
                    }            
                },
                {
                    data : function(data) {
                        return moment(data.created_at).format('MMMM D, YYYY');
                    }            
                },
                { 
                    defaultContent: '', createdCell: function (td, cellData, rowData, row, col) {

                    $(td).html('<button class="btn btn-primary btn-sm flood-edit-btn"><i class="fa fa-pencil-alt"></i></button> \
                                <button class="btn btn-danger btn-sm flood-delete-btn"  data-id="'+rowData.flood_id+'"><i class="fa fa-times"></i></button>');
                    }
                }
        ]
    });

$(document).on('click','#addeditfloodRecord', function() {
  $('#formFloodRecord')[0].reset();
});

$('#flood-record-tbl tbody').on('click','.flood-edit-btn', function() {
  $('#addEditFloodRecordModal').modal('show');
  var data = tblfloodrecord.row($(this).parent()).data();

  $('input[name=flood_id]').val(data.flood_id);
  $('input[name=flood_name]').val(data.flood_name);
  $('input[name=flood_desc]').val(data.flood_desc);
  $('input[name=start]').val(moment.utc(data.start).local().format("YYYY-MM-DDTHH:mm"));
  $('input[name=end]').val(moment.utc(data.end).local().format("YYYY-MM-DDTHH:mm"));
  $('input[name=date_happen]').val(data.date_happen);

});




function updateLatLng(lat,lng,reverse) {
  if(reverse) {
  marker.setLatLng([lat,lng]);
  map.panTo([lat,lng]);
  } else {
  document.getElementById('latitude').value = marker.getLatLng().lat;
  document.getElementById('longitude').value = marker.getLatLng().lng;
  map.panTo([lat,lng]);
  }

}

function disableMarker() {
  marker_icon = "fa fa-map-marker-alt";
  mymarker.forEach(function(item, index, arr){
    mymarker[index].dragging.disable();
  });
}

function deleteMarker(marker_id) {
  mymarker[marker_id].remove();
  index = mymarker.indexOf(marker_id);
  mymarker.splice(index,1);
  marker_count = mymarker.length;

}


function deletePolygon() {

  mypolygon.remove();
  mypolygon_arr.length = 0;
  polygon_count = 0;
  mypolygon = null;
}
</script>
</body>