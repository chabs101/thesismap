import * as env from '../env.js';
const URI = env.url;

export default {


    async pullData(ext, arr = {} ) {

        var opt = {
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method : 'GET'
        };

        try {

                let response = await fetch(URI + ext,opt);
                const statusCode = response.status;
                let responseJsonData = await response.json();
                return {status : statusCode, data: responseJsonData, error:false};

            }
        catch(e) {
            return {error : true, description : e};
        }

    }
}