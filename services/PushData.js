import * as env from '../env.js';
const URI = env.url;

export default {


    async pushData(ext, arr = [] ) {

        var opt = {
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method : 'POST',
            body : JSON.stringify(arr)
        };

        try {
                let response = await fetch(URI + ext,opt);
                const statusCode = response.status;
                let responseJsonData = await response.json();   
                return {status : statusCode, data: responseJsonData, error:false};

            }
        catch(e) {
            console.log(e)
            return {error : true, description : e};

        }

    }
}