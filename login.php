<head>
<link rel="stylesheet" href="./assets/leaflet-js/leaflet.css" />
<!-- <link rel="stylesheet" href="./assets/bootstrap-native/assets/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="./assets/bootstrap-4.4.1-dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="./assets/fontawesome/css/all.css"/>
<link rel="stylesheet" href="./assets/leaflet-routing-machine/dist/leaflet-routing-machine.css"/>
<style>
  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }
  html,body {
    margin :0;
    overflow-y: hidden;
    overflow-x: hidden;
  }

  body {
    background-image: url('./assets/world_map_bg.jpg');
    background-size: 100%;
  }

  #loginModal > .modal-dialog > .modal-content {
    background-image: url('./assets/world_map_bg.jpg');
    background-size: 100%;
    color: #fff;
    border: 2px solid #d39e00;
  }

  #mymap {
    left:0;
    top:0;
      width: 100%;
      height: 100%;
      position: absolute;
   }

   .left-tools{
    border-radius: 5px 0px 0px 5px;
    right:0;
    height:auto;
    width:50px;
    position:absolute;
    z-index:500;
    margin-top: 5%;
  }

  @keyframes fade { 
    from { opacity: 0.5; } 
  }
  .navbar {
    z-index: 500;
  }
  .blinking {
    animation: pulse 1s infinite alternate;
  }

  @keyframes pulse {
      0% {
        /*fill: rgba(255,0,0,.3);*/
        opacity: 3;
      }
      100% {
        stroke-width:2px;
        /*fill: rgba(255,0,0,0.9);*/
        opacity: .4;
      }
  }

  .btn-zoom {
    width:50px;
    height:50px;
    font-size: 25px;
    border: 0;
  }

  .btn-zoom:active:focus {
    outline: none;
    border: 0;
    box-shadow: none;
  }
  .navbar-nav, a:hover {
    text-decoration: none;
    color: white;
  }
  .navbar-nav, a {
    color: white;
  }
  .modal-left {
    min-width: 0;
    width: auto !important;
  }

  .modal-left .modal-dialog,
  .modal.right .modal-dialog {
    margin: auto;
    width: 320px;
    height: 100%;
    -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
         -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
  }

  .modal-left .modal-content,
  .modal.right .modal-content {
    height: 100%;
    overflow-y: auto;
  }
  
  .modal-left .modal-body,
  .modal.right .modal-body {
    padding: 15px 15px 80px;
  }

  .modal-left.fade .modal-dialog{
    left: 0;
    -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
       -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
         -o-transition: opacity 0.3s linear, left 0.3s ease-out;
            transition: opacity 0.3s linear, left 0.3s ease-out;
  }

  .my-label {
  position: absolute;
  font-size:13px;
  font-weight: bold;
  letter-spacing: 0.2px;
  color:#0062cc !important;
  background-color:transparent !important;
  box-shadow:none;
  border:none;
  }

  .leaflet-tooltip-right:before {
    border-right-color:#0062cc !important;
  }

  .m-icon {
    color:#0062cc !important;
  }

</style>
</head>
<body>
<!-- <nav class="navbar bg-dark navbar-dark">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Barangay Manambia</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="#">Map</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li>
        <a href="#" data-toggle="modal" data-target="#loginModal" id="loginBtn">
          <i class="fa fa-user"></i> Login
        </a>
      </li>
    </ul>
  </div>
</nav> -->



<!-- Modal Login -->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static" style="margin: 2% auto !important;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <img src="./assets/default_pic.png" style="height: auto;width: 150px;margin: 1rem auto;" />
      </div>
      <div class="modal-body">
        <h5 style="color:red;" id="invalid_credentials">Invalid Username or Password.</h5>
        <form id="formLogin">
          <div class="form-group">
            <label for="exampleInputUsername">Username</label>
            <input type="text" class="form-control" id="input_username" name="input_username" placeholder="Username">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="input_password" name="input_password" placeholder="Password">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-warning" style="color:white;" id="modalLoginBtn">Login</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Login End-->


<script src="./assets/leaflet-js/leaflet.js"></script>
<script src="./assets/leaflet-js/leaflet-pip.js"></script>
<script src="./assets/leaflet-routing-machine/dist/leaflet-routing-machine.js"></script>
<!-- <script src="bootstrap-native/assets/js/bootstrap-native.js"></script> -->
<script src="./assets/bootstrap-4.4.1-dist/js/jquery.js"></script>
<script src="./assets/bootstrap-4.4.1-dist/js/bootstrap.js"></script>
<script src="./node_modules/socket.io-client/dist/socket.io.js"></script>

<script>
  var marker_icon = "fa fa-map-marker-alt";
  var marker_name = $('#markerNameId');
  var markerfromdb = [];
  var polygonfromdb = [];
</script>

<script type="module">
  import PushData from './services/PushData.js';
  import PullData from './services/PullData.js';
  import Auth from './services/Authenticate.js';
  import * as env from './env.js';

  $(document).on('keyup','#input_password, #input_username', function(e){
    if(e.keyCode == 13) {
      $('#modalLoginBtn').click();
    }
  });

  $('#invalid_credentials').hide();
  $('#loginModal').modal("show");
  $('.modal-backdrop').removeClass('modal-backdrop');
  $('#modalLoginBtn').on('click', async function() {
    var arr = {};
    var input_username     = document.getElementById('input_username');
    var input_password  = document.getElementById('input_password');

    arr["input_username"]    = input_username.value;
    arr["input_password"] = input_password.value;

    // console.log(arr)
    const callback = await Auth.authenticate('./controller/api/authenticate.php',arr);
    console.log(callback)
        if(!callback.error) {
          if(callback.data[0].success) {
            $('#invalid_credentials').hide();
            $('#loginModal').modal('hide');
            document.getElementById("formLogin").reset();
            setTimeout(function()
            {
                var redirect_url = "view/edit-map";
                
                window.location.href = redirect_url;
            }, 400);

          }else {
            $('#invalid_credentials').show();

          }
        }
  });


</script>
</body>