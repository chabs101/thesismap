var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 4000;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	console.log("Connected!");

	socket.on('realtimeData', function(data){
		io.emit('realtimeData', data);
	});
	
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
