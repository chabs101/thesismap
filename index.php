<?php
  session_start();
  if(!isset($_SESSION['fullname'])) {
    header('Location:./login.php');
  }

?>
<head>
<link rel="stylesheet" href="./assets/leaflet-js/leaflet.css" />
<!-- <link rel="stylesheet" href="./assets/bootstrap-native/assets/css/bootstrap.min.css"/> -->
<link rel="stylesheet" href="./assets/bootstrap-4.4.1-dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="./assets/fontawesome/css/all.css"/>
<link rel="stylesheet" href="./assets/leaflet-routing-machine/dist/leaflet-routing-machine.css"/>
<style>
  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }
  html,body {
    margin :0;
    overflow-y: hidden;
    overflow-x: hidden;
  }
  #mymap {
    left:0;
    top:0;
      width: 100%;
      height: 100%;
      position: absolute;
   }

   .left-tools{
    border-radius: 5px 0px 0px 5px;
    right:0;
    height:auto;
    width:50px;
    position:absolute;
    z-index:500;
    margin-top: 5%;
  }

  @keyframes fade { 
    from { opacity: 0.5; } 
  }
  .navbar {
    z-index: 500;
  }
  .blinking {
    animation: pulse 1s infinite alternate;
  }

  @keyframes pulse {
      0% {
        /*fill: rgba(255,0,0,.3);*/
        opacity: 3;
      }
      100% {
        stroke-width:2px;
        /*fill: rgba(255,0,0,0.9);*/
        opacity: .4;
      }
  }

  .btn-zoom {
    width:50px;
    height:50px;
    font-size: 25px;
    border: 0;
  }

  .btn-zoom:active:focus {
    outline: none;
    border: 0;
    box-shadow: none;
  }
  .navbar-nav, a:hover {
    text-decoration: none;
    color: white;
  }
  .navbar-nav, a {
    color: white;
  }
  .modal-left {
    min-width: 0;
    width: auto !important;
  }

  .modal-left .modal-dialog,
  .modal.right .modal-dialog {
    margin: auto;
    width: 320px;
    height: 100%;
    -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
         -o-transform: translate3d(0%, 0, 0);
            transform: translate3d(0%, 0, 0);
  }

  .modal-left .modal-content,
  .modal.right .modal-content {
    height: 100%;
    overflow-y: auto;
  }
  
  .modal-left .modal-body,
  .modal.right .modal-body {
    padding: 15px 15px 80px;
  }

  .modal-left.fade .modal-dialog{
    left: 0;
    -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
       -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
         -o-transition: opacity 0.3s linear, left 0.3s ease-out;
            transition: opacity 0.3s linear, left 0.3s ease-out;
  }

  .my-label {
  position: absolute;
  font-size:13px;
  font-weight: bold;
  letter-spacing: 0.2px;
  color:#0062cc !important;
  background-color:transparent !important;
  box-shadow:none;
  border:none;
  }

  .leaflet-tooltip-right:before {
    border-right-color:#0062cc !important;
  }

  .m-icon {
    color:#0062cc !important;
  }

  .navbar-right {
    flex-direction: row !important;
  }

  .navbar-right > li {
    float:right;
    padding: 5;
  }

  .color-orange {
    color:ff8000;
  }

</style>
</head>
<body>
<nav class="navbar bg-dark navbar-dark">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/thesismap">Barangay Manambia</a>
    </div>
<!--     <ul class="nav navbar-nav">
      <li><a href="#">Map</a></li>
    </ul> -->
    <ul class="nav navbar-nav navbar-right">
      <li>
<!--         <a href="#" data-toggle="modal" data-target="#loginModal" id="loginBtn">
          <i class="fa fa-user"></i> Login
        </a> -->
        <a href="./view/edit-map.php" id="loginBtn">
          <i class="fa fa-edit color-orange"></i> Edit Map
        </a>
      </li>

        <li>
          <a href="./controller/api/session-destroy.php"><i class="fa fa-sign-out-alt color-orange" ></i> Log Out</a>
        </li>
    </ul>
  </div>
</nav>

<div class="container-fluid">
  <div id="mymap"></div>
<div class="bg-dark left-tools">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" title="Zoom In" id='zoomIn'>+</button>
    </div>
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-md btn-zoom" title="Zoom Out" id='zoomOut'>-</button>
    </div>
  </div>
</div>
<div class="bg-dark left-tools" style="margin-top: 13%; ">
  <div class="row">
    <div class="col-md-12">
      <button type="button" class="btn btn-dark btn-sm btn-zoom" data-toggle="modal" data-target="#mapKeyModal" title="Map Keys" id="mapKeyId">
        <span class="fa fa-info fa-sm"></span>
      </button>
    </div>
  </div>
</div>

</div>

  <div class="modal modal-left fade" id="mapKeyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Map Keys</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="exampleInputEmail1"><b>Flood Level Indicator cm/feet</b></label>
              <div class="row">
                <div class="col-sm-12">                
                  <span class="badge" style="background-color: #ff0000;color:white;">Extreme</span><br>
                  225cm-300cm above / 7.4ft-9.8 above<br><br>
                </div>
                <div class="col-sm-12">
                  <span class="badge" style="background-color: #ff8000;">Severe</span><br>
                  150cm - 224cm / 4.9ft-7.4ft <br><br>
                </div>
                <div class="col-sm-12">
                  <span class="badge" style="background-color: #ffff00;">Above Normal</span><br>
                   38cm - 149cm / 1.22ft-4.9ft <br><br>
                </div>
                <div class="col-sm-12">
                  <span class="badge" style="background-color: #00ff00;">normal</span><br>
                   18.75cm-37cm / 0.616ft-1.21ft <br><br>
                </div>
                <div class="col-sm-12">
                  <span class="badge" style="background-color: #00bfff;">below normal</span><br>
                   0cm-18.75cm / 0ft-0.616ft<br><br>
                </div>
              </div>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1"><b>Marker Icon</b></label>
              <div class="form-group">
                <div class="row">

                    <div class="col-sm-12">
                      <label class="form-check-label"><span class="fa fa-home"></span></label>
                      Home<br><br>
                    </div>

                    <div class="col-sm-12">
                      <label class="form-check-label"><span class="fa fa-store-alt"></span></label>
                      Store<br><br>
                    </div>

                    <div class="col-sm-12">
                      <label class="form-check-label"><span class="fa fa-map-marker-alt"></span></label>
                      Marker<br><br>
                    </div>

                </div>
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>

  <div class="modal modal-left fade" id="affectedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Household Affected</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="modal-body">
          <form>
            <div class="form-group">
              <div class="row" id="affectedList">

              </div>
              <!-- <div class="row">
                <div class="col-sm-12"> 
                    <div id="collapse-name" data-toggle="collapse" data-target="#test">asdasdasdsa</div>
                    <div class="collapse" id="test">asdasdklasdklasdklasdklasdklas</div>
                    </div>
                <div class="col-sm-12"> 
                    <div id="collapse-name" data-toggle="collapse" data-target="#test-1">asdasdasdsa</div>
                    <div class="collapse" id="test-1">asdasdklasdklasdklasdklasdklas</div>
                    </div>
              </div> -->
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>


<!-- Modal Login -->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title text-center" id="exampleModalLabel">Login</h2>
      </div>
      <div class="modal-body">
        <h5 style="color:red;" id="invalid_credentials">Invalid Username or Password.</h5>
        <form id="formLogin">
          <div class="form-group">
            <label for="exampleInputUsername">Username</label>
            <input type="text" class="form-control" id="input_username" name="input_username" placeholder="Username">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="input_password" name="input_password" placeholder="Password">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="modalLoginBtn">Login</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Login End-->


<script src="./assets/leaflet-js/leaflet.js"></script>
<script src="./assets/leaflet-js/leaflet-pip.js"></script>
<script src="./assets/leaflet-routing-machine/dist/leaflet-routing-machine.js"></script>
<!-- <script src="bootstrap-native/assets/js/bootstrap-native.js"></script> -->
<script src="./assets/bootstrap-4.4.1-dist/js/jquery.js"></script>
<script src="./assets/bootstrap-4.4.1-dist/js/bootstrap.js"></script>
<script src="./node_modules/socket.io-client/dist/socket.io.js"></script>

<script>
  var marker_icon = "fa fa-map-marker-alt";
  var marker_name = $('#markerNameId');
  var markerfromdb = [];
  var polygonfromdb = [];
</script>

<script type="module">
  import PushData from './services/PushData.js';
  import PullData from './services/PullData.js';
  import Auth from './services/Authenticate.js';
  import * as env from './env.js';

  $(document).on('keyup','#input_password, #input_username', function(e){
    if(e.keyCode == 13) {
      $('#modalLoginBtn').click();
    }
  });

  $('#invalid_credentials').hide();
  $("#mapKeyModal").modal("show");
  $('.modal-backdrop').removeClass('modal-backdrop');
  $('#modalLoginBtn').on('click', async function() {
    var arr = {};
    var input_username     = document.getElementById('input_username');
    var input_password  = document.getElementById('input_password');

    arr["input_username"]    = input_username.value;
    arr["input_password"] = input_password.value;

    console.log(arr)
    const callback = await Auth.authenticate('/controller/api/authenticate.php',arr);

        if(!callback.error) {
          if(callback.data[0].success) {
            $('#invalid_credentials').hide();
            $('#loginModal').modal('hide');
            document.getElementById("formLogin").reset();
            setTimeout(function()
            {
                var redirect_url = "view/edit-map";
                
                window.location.href = redirect_url;
            }, 400);

          }else {
            $('#invalid_credentials').show();

          }
        }
  });



  const getmarker = async() => {
    const callbackPull = await PullData.pullData('/controller/api/get-all-marker.php');

        if(!callbackPull.error) {

            callbackPull.data.forEach( async function(item, index, arr) {
              // console.log(JSON.parse([callbackPull.data[index].latlng]));
              var icon        = callbackPull.data[index].icon;
              var markername  = callbackPull.data[index].marker_name;
              var latlng      = [];
              var purokname   = callbackPull.data[index].purok_name ? "(" + callbackPull.data[index].purok_name + ") - " : "";
              var _latlng     = JSON.parse(callbackPull.data[index].latlng);
              var marker_id   = callbackPull.data[index].marker_id;
              var person      = callbackPull.data[index][0].person;
              var description = callbackPull.data[index].description ? callbackPull.data[index].description : "no details<br>";

              for(var parse in _latlng) {
                latlng.push(_latlng[parse]);
              }

              var faIcon = L.divIcon({
                  html: icon,
                  className: 'myDivIcon'
              });

              markerfromdb[index] = new L.Marker(latlng, {
                           icon : faIcon
                         });

               markerfromdb[index].addTo(map);
               markerfromdb[index].purok_name  = purokname;
               markerfromdb[index].icon        = icon;
               markerfromdb[index].marker_name = markername;
               markerfromdb[index].description = description;
               markerfromdb[index].person      = person;
               markerfromdb[index].bindTooltip(purokname+markername, {permanent: true, className: "my-label", direction:'right',offset:[10,0] });
                markerfromdb[index].bindPopup(description);

            });
          }else{
            console.log("fail");
          }
        
  }

  const getpolygon = async() => {
    const callbackPull = await PullData.pullData('/controller/api/get-all-polygon.php');

        if(!callbackPull.error) {

            callbackPull.data.forEach( async function(item, index, arr) {
              var polygon_id  = callbackPull.data[index].polygon_id;
              var _latlng     = JSON.parse(callbackPull.data[index].latlng);
              var description = callbackPull.data[index].description ? callbackPull.data[index].description : "";
              var distance_color = callbackPull.data[index].distance == 18 ? "#00bfff" : (callbackPull.data[index].distance == 37 ? "#00ff00" : callbackPull.data[index].distance == 149 ? "#ffff00" : callbackPull.data[index].distance == 224 ? "#ff8000" : "#ff0000" );
              polygonfromdb[index] = L.polygon(_latlng)
              .setStyle({className:'polygon_'+polygon_id,color:distance_color,fill:distance_color,weight:0.5})
              .bindPopup(description + '<br><button class="btn btn-danger btn-sm btn-show-affected" data-index='+index+'>SHOW AFFECTED HOUSEHOLD</button>')
              .addTo(map);
              $('.polygon_'+polygon_id).addClass("blinking");

            });
          }else{
            console.log("fail");
          }
        
  }

  getmarker();
  getpolygon();
  $("#mapKeyId").on('click', function() {
      setTimeout(()=> {
      $('.modal-backdrop').removeClass('modal-backdrop');
      $('.modal').modal('hide');
      },0.5);
  });

// maps -------------------

var map = L.map('mymap',{
    center: [8.4351,126.1442],
    scrollWheelZoom: false,
    zoomControl: false,
    zoom: 14
});

if(window.navigator.onLine) {
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}{r}.png', {
      attribution: '© OpenStreetMap contributors'
  }).addTo(map);
}else {
  L.tileLayer('./assets/map/{z}/{x}/{y}.png',{
      minZoom:14,
      maxZoom:17
  }).addTo(map);
}

// L.Routing.control({
//     waypoints: [
//         L.latLng(8.440196105295872, 126.14330291748048),
//         L.latLng(8.431536095186294, 126.14218711853029)
//     ],
//     routeWhileDragging: true
// }).addTo(map);

document.getElementById('zoomIn').addEventListener('click', function () {
    map.setZoom(map.getZoom() + 1);
});

document.getElementById('zoomOut').addEventListener('click', function () {
    map.setZoom(map.getZoom() - 1);
});

map.on('click', function(e){
  var coord = e.latlng;
  var lat = coord.lat;
  var lng = coord.lng;

  
  console.log("You clicked the map at latitude: " + lat + " and longitude: " + lng);

  
  });

var socket = io(":4000");
// #ff0000 major
// #ff8000 moderate
// #ffff00 minor
// #00ff00 normal
// #00bfff

socket.on('realtimeData', function(callback){
  // console.log(callback.data.indicator);
  var indicator = callback.data.indicator;
  var distance  = callback.data.distance;
  var maxMeter = 300;
  var minMeter = parseFloat(maxMeter)/4;
  console.log(callback);
  if(distance < (minMeter/2)/2) {
    console.log("1 : "+distance);
    $('.polygon_'+indicator).attr({stroke:'#00bfff',fill:'#00bfff'}).removeClass('blinking');
  }
  if(distance >= (minMeter/2)/2 && distance<(minMeter/2)) {
    console.log("2 : "+distance);
    $('.polygon_'+indicator).attr({stroke:'#00ff00',fill:'#00ff00'}).removeClass('blinking').addClass('blinking');
  }
  if(distance >= (minMeter/2) && distance<(minMeter*2)) {
    console.log("3 : "+distance);
    console.log("3 : "+indicator);
    $('.polygon_'+indicator).attr({stroke:'#ffff00',fill:'#ffff00'}).removeClass('blinking').addClass('blinking');
  }
  if(distance >= (minMeter*2) && distance<(minMeter*3)) {
    console.log("4 : "+distance);
    $('.polygon_'+indicator).attr({stroke:'#ff8000',fill:'#ff8000'}).removeClass('blinking').addClass('blinking');    
  }
  if(distance >= (minMeter*3)) {
    console.log("5 : "+distance);
    $('.polygon_'+indicator).attr({stroke:'#ff0000',fill:'#ff0000'}).removeClass('blinking').addClass('blinking');        
  }

});


setTimeout(()=> {
  console.log(polygonfromdb[1].contains(markerfromdb[10].getLatLng())); //true 
  //loop polygon then loop marker. get all details from marker that are true
    markerfromdb.forEach(function(item, index, arr) {
      var iterate = markerfromdb[index];

      if( polygonfromdb[0].contains(iterate.getLatLng()) ) {
        console.log(iterate.marker_name);
      }
    });
},3000);

$(document).on('click','.btn-show-affected', function() {
    var polygonindex = $(this).attr('data-index');
    $('#affectedModal').modal('show');
    $('.modal-backdrop').removeClass('modal-backdrop');
    $('#affectedList').html("");
    markerfromdb.forEach(function(item, index, arr) {
      var iterate = markerfromdb[index];

      if( polygonfromdb[polygonindex].contains(iterate.getLatLng()) ) {
        console.log(iterate.marker_name);
        var collapseVar = 'collapse-'+index;
        var template = $('<div class="col-sm-12"> \
                    <div class="collapse-name" data-click="0" data-toggle="collapse" data-target='+'#'+collapseVar+' style="margin: 5px;padding: 5px;"></div>\
                    <div class="collapse" id='+collapseVar+' style="border: .5px solid rgb(137, 125, 125);border-radius: 5px;padding: 5px;margin: 5px;font-size:12px;"><span style="font-weight:bold;">Household Member :</span> <table style="font-size:12px;"><tbody></tbody></table><span style="font-weight:bold;">Other Details :</span>  <div class="description"></div>\
                    </div>\
                    </div>');
        var person = "";
        iterate.person.forEach(function(item) {
          person += '<tr>\
                        <td>Name</td>\
                        <td> : '+item.name+'</td>\
                    </tr>\
                    <tr>\
                        <td>Age</td>\
                        <td> : '+item.age+'</td>\
                    </tr>';

        });
        var warning = ""; 
        iterate.person.filter(function(item) {
          const age = item.age;
            if( age >= 60) {
              warning = '<span class="fa fa-exclamation-triangle" style="color:red;" title="Indicate that there`s senior citizen or PWD that needs to be assist. "></span>';
            }
        });

        template.find(".collapse-name").html(warning +" "+ iterate.icon.replace("m-icon","") +" "+ iterate.marker_name+"<span class='fa fa-angle-left' style='float:right;'></span><br>");
        console.log(iterate.person);
        template.find("tbody").html(person);
        template.find(".description").html(iterate.description);
        template.appendTo($('#affectedList'));
      }
    });
});

$(document).on('click','.collapse-name', function() {
  if($(this).attr('data-click') == 0) {
    $(this).find('span').removeClass('fa-angle-left').addClass('fa-angle-down');
    $(this).attr('data-click',1);
  }else {
    $(this).find('span').removeClass('fa-angle-down').addClass('fa-angle-left');
    $(this).attr('data-click',0);
  }
  // console.log($(this).find('span'));
});

</script>
</body>